public class aula20 {  // laços condicionais

    public static void main(String[] args) {
        int x = 0;  // variavel inteira valor 0

        while (x<=9) { // X menor ou igual a 9
            x = x + 1;  // valor de x + 1

            // 10 vezes o valor foi impresso, contando a partir do 0

            System.out.println("Olá, eu sou um looping!"); // imprime a mensagem 10 vezes
        }
    }
}
